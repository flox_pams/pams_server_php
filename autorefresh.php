<?php
ob_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);
$settings = simplexml_load_file("internal/settings.xml");
if ($settings->new_status_notif->enabled == "true"){}else{
    echo "Option disabled, check config";
    exit();
}
?>
<script>
var ch = new BroadcastChannel('pams_autorefresh_<?php echo $settings->new_status_notif->js_broadcastchannel_id; ?>');
</script>

<?php

function gethash(){
    $hash = file_get_contents("internal/data.xml");

    $hash1 = explode("</lastcheck>", $hash)[1];
    $hash2 = explode("<lastcheck>", $hash)[0];
    $hash = $hash2 . $hash1;
    
    
    $nbvar = substr_count($hash, '<lastping>');
    for ($i = 1; $i <= $nbvar; $i++) {
        $varchange = explode("<lastping>", $hash);
        $varchange = explode("</lastping>", $varchange[1]);
        $varchange = $varchange[0];
        $hash = str_replace('<lastping>' . $varchange . '</lastping>', "", $hash);
        //$nbvar = (integer)$i + (integer)substr_count($hash, '<lastping>');
    }
    return md5($hash);
}

$hashget = gethash();
//echo substr_count($hash, '<lastping>');
echo htmlspecialchars($hashget);

if (isset($_GET['hash'])){
    if ($hashget == $_GET["hash"]){
        echo "nochange";
        echo '<meta http-equiv="refresh" content="' .  $settings->new_status_notif->refresh_rate . ';">';
    }else{
        echo 'CHANGE DETECTED';
        echo "<script>ch.postMessage('true');</script>";
    }

}else{
    header('Location: autorefresh?hash=' . $hashget);
    exit();  
}