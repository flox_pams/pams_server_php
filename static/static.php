<?php
$page_title = $settings->page_title->en;
$bar_title = $settings->bar_txt->en;

if ($_SESSION['lang'] == "fr"){
    $page_title = $settings->page_title->fr;
    $bar_title = $settings->bar_txt->fr;
}
?>
<head>
    <title><?php echo $page_title; ?></title>
    <link rel="stylesheet" href="static/flox.css">
    <link rel="stylesheet" href="static/main.css">
    <link rel="stylesheet" href="static/fontawesome/css/all.css">
    <link rel="stylesheet" href="/dist/notify/css/notify.css">
    <link rel="stylesheet" href="/dist/notify/css/prettify.css">
    <script type="text/javascript" src="/dist/jquery.js"></script>
    <script type="text/javascript" src="/dist/notify/js/notify.js"></script>
    <script type="text/javascript" src="/dist/notify/js/prettify.js"></script>
</head>
<div class="flox-bar flox-black">
    <a href="index.php">
        <?php if ((string)$settings->logo->enabled == "true"){ ?>
            <div class="flox-bar-item"><img src="<?php echo $settings->logo->url; ?>" style="margin-top: <?php echo $settings->logo->top_offset; ?>px;" height=25px  alt="Logo"></div>
        <?php } ?>
        <div class="flox-bar-item" style="font-size: 16px;"><?php echo $bar_title; ?></div>
    </a>
    <?php if ((string)$settings->language->show_lang_selector == "true"){ ?>
        <div class="flox-dropdown-hover flox-right" style="margin-top: 3px;">
            <button class="flox-button" style="font-size: 19px;"><i class='fa fa-globe'></i></button>
            <div class="flox-dropdown-content flox-bar-block flox-card-4" style="right: 0px !important;">
                <a href="?lang=fr" class="flox-bar-item flox-button flox-left">French</a>
                <a href="?lang=en" class="flox-bar-item flox-button flox-left">English</a>
            </div>
        </div>
    <?php } ?>
</div><br><br>
