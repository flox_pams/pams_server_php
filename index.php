<?php 
ob_start();
$settings = simplexml_load_file("internal/settings.xml");
if (isset($_GET["admin"])){
    require("internal/admin_login.php");
    do_login($settings);
}else{
    if ((string)$settings->admin_login->disable_public_access == "true"){
        header("Location: ?admin");
        exit();
    }
}
@session_start();
if ($_GET["lang"] == "fr") {
	$_SESSION['lang'] = "fr";
}
if ($_GET["lang"] == "en") {
	$_SESSION['lang'] = "en";
}
if (!isset($_SESSION['lang'])){
    $_SESSION['lang'] = (string)$settings->language->default_language;
}
//lang
$lastcheck = "Last Automatic Check";
$msg_new = "The status has been updated, click here to refresh the page.";
$page_footer = $settings->footer->en;

if ($_SESSION['lang'] == "fr"){
  $lastcheck = "Dernière verification automatique";
  $msg_new = "Une mise à jour a été apporté au statut, cliquez ici pour rafraichir la page.";
  $page_footer = $settings->footer->fr;
}

$file = "internal/data.xml";
$data = simplexml_load_file($file);
$json_string = json_encode($data);
$array = json_decode($json_string, true);
require("static/static.php");
?>
<div style="padding-left:1em; width: 100%">
    <div style="max-width: 1000px; margin-left: auto; margin-right: auto; padding-left:-1em;" class="flox-round-xlarge flox-text-white"><?php echo $lastcheck; ?>: <?php echo $data->lastcheck; ?></div>
</div>

<?php
$animation = "right";
foreach ($array as $service) {
    $arrayid = array_search($service, $array);
    $is_cat = explode("_", $arrayid)[0];
    if ($is_cat == 'cat'){
        $cat_name = $service["name_en"];
        if ($_SESSION['lang'] == "fr"){
            $cat_name = $service["name_fr"];
        }
        ?>

            <div style="padding:1em;">
            <div style="max-width: 1000px; bottom: -10px; margin-left: auto; margin-right: auto;" class="flox-animate-<?php echo $animation; ?>">

            <div class="flox-text-white">
                    <div  class="flox-left-align">   
                    <font size=5px><?php echo $cat_name; ?> :</font>
                    </div>
                </a>
            </div>

            </div>
            </div>
        <?php
    }elseif($arrayid == "lastcheck"){
    }else{

    if ($animation == "right"){
        $animation = "left";
    }else{
        $animation = "right";
    }

    $lastping = $service["lastping"];
    if ($lastping > 900){
        $lastping = $lastping - 900;
    }


    if ($service["mode"] == "auto_ping"){
        //mode auto avec ping
        if ($service["status"] == "true"){
            $item_color = "flox-green";
            $item_txt_right = "<i class='fa fa-check'></i> - " . $lastping . "ms&nbsp;";
        }else{
            $item_color = "flox-red";
            $item_txt_right =  "<i class='fa fa-times'></i>&nbsp;&nbsp;";
        }
    }elseif ($service["mode"] == "auto"){
        //mode auto 
        if ($service["status"] == "true"){
            $item_color = "flox-green";
            $item_txt_right = "<i class='fa fa-check'></i>&nbsp;&nbsp;";
        }else{
            $item_color = "flox-red";
            $item_txt_right =  "<i class='fa fa-times'></i>&nbsp;&nbsp;";
        }
    }elseif ($service["mode"] == "warn_ping"){
        //Mode warning avec ping
        $item_color = "flox-orange";
        $item_txt_right = "<i class='fa fa-exclamation'></i> - " . $lastping . "ms&nbsp;";
    }elseif ($service["mode"] == "warn"){
        //Mode warning
        $item_color = "flox-orange";
        $item_txt_right = "<i class='fa fa-exclamation'></i>&nbsp;&nbsp;";
    }elseif ($service["mode"] == "force_offline"){
        //mode offline
        $item_color = "flox-red";
        $item_txt_right =  "<i class='fa fa-times'></i>&nbsp;&nbsp;";
    }elseif ($service["mode"] == "force_online_ping"){
        //mode en ligne avec ping
        $item_color = "flox-green";
        $item_txt_right = "<i class='fa fa-check'></i> - " . $lastping . "ms&nbsp;";
    }elseif ($service["mode"] == "force_online"){
        //mode en ligne
        $item_color = "flox-green";
        $item_txt_right =  "<i class='fa fa-check'></i>&nbsp;&nbsp;";
    }elseif ($service["mode"] == "maintenance_ping"){
        //Mode warning avec ping
        $item_color = "flox-blue";
        $item_txt_right = "<i class='fa fa-wrench'></i> - " . $lastping . "ms&nbsp;";
    }elseif ($service["mode"] == "maintenance"){
        //Mode warning
        $item_color = "flox-blue";
        $item_txt_right = "<i class='fa fa-wrench'></i>&nbsp;&nbsp;";
    }



    $item_txt = $service["msgen"];
    $item_name = $service["name_en"];
    if ($_SESSION['lang'] == "fr"){
        $item_txt = $service["msgfr"];
        $item_name = $service["name_fr"];
    }
    if ($item_txt == "[none]"){
        $item_txt = "";
    }


    ?>

    <div style="padding:1em;">
    <div style="max-width: 1000px; margin-left: auto; margin-right: auto;" class="<?php echo $item_color; ?> flox-round-xlarge shadow flox-animate-<?php echo $animation; ?>">

        <div class="flox-text-white flox-margin-left">
        <?php if (isset($_GET['admin'])){ ?>
            <a href="admin.php?id=<?php echo $arrayid; ?>">
        <?php } ?>
                <div style="padding:5px;" class="flox-left-align">
                <table style="width: 100%;">
                    <tr>
                        <td ><font size=5px class="flox-text-white"><i class="<?php echo $service["fa"]; ?>"></i>&nbsp;<?php echo $item_name ?></font></td>
                        <td><div class="flox-right-align flox-text-white"><?php echo $item_txt_right ?></div></td>
                    </tr>
                </table>
                    
                    <font size=2px><?php echo $item_txt; ?></font>
                </div>

            </a>
        </div>

    </div>
    </div>

<?php
    }
}

if (isset($_GET['admin'])){
    ?>
    <div class="flox-center"><a href="settings.php" class="flox-center flox-text-white">Advanced settings</a></div>
    <?php
}
?>
<iframe id="spacer"
    width="1"
    height="50"
    src="about:blank"
    frameborder="0">
</iframe>
<div class="flox-center flox-text-white flox-margin-bottom"><?php echo $page_footer; ?></div>

<?php if ($settings->new_status_notif->enabled == "true"){ ?>
<iframe id="autorefresh" width="1" height="1" src="autorefresh.php" frameborder="0"></iframe>
<script>
var ch = new BroadcastChannel('pams_autorefresh_<?php echo $settings->new_status_notif->js_broadcastchannel_id; ?>');
ch.addEventListener('message', function (e) {
    console.log('Message:', e.data);
    $.notify("&nbsp;&nbsp;<a class='w3-text-white' href=\"\"><?php echo $msg_new; ?></a>", {blur: 0.0, align:"right", verticalAlign:"top", type: "info", icon:"exclamation", background: "#A5881B", close: true, delay:0});
});
</script>
<?php } ?>