<?php
$settings = simplexml_load_file("settings.xml");
date_default_timezone_set((string)$settings->timezone);
include("notifs/" . (string)$settings->notification_handler . ".php");
function milliseconds() {
    $mt = explode(' ', microtime());
    return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 100));
}
$file = "data.xml";
$data = simplexml_load_file($file);
$json_string = json_encode($data);
$array = json_decode($json_string, true);
foreach ($array as $service) {
    $arrayid = array_search($service, $array);
    $is_cat = explode("_", $arrayid)[0];
    if ($is_cat == 'cat'){
    }elseif($arrayid == "lastcheck"){
    }else{
        if ($data->$arrayid->probe == "curl"){
            $start_time = milliseconds();
            $curl_handle = curl_init();
            $url = $service["address"];
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($curl_handle, CURLOPT_TIMEOUT, 10);
            $curl_data = curl_exec($curl_handle);
            $response_code = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
            curl_close($curl_handle);
            if ($response_code == $service["http_response_ok"])
            {
                $ping = milliseconds() - $start_time;
                send_notifs($service["name_en"], $data->$arrayid->status, "true", $ping );
                $data->$arrayid->status = "true";
                $data->$arrayid->lastping = $ping;
            }
            else
            {
                $ping = "unr";
                send_notifs($service["name_en"],$data->$arrayid->status, "false" );
                $data->$arrayid->status = "false";
            }

            echo $arrayid . " " . $service["name_en"] . " " . $service["address"] . " " . $service["probe"] . $ping . "\n\n";
        }
        if ($data->$arrayid->probe == "icmp"){
            $str = exec("ping -c " . $service["count"] . " -W 1 " . $service["address"]);
            if ($str == ""){
                $ping = "unr";
                send_notifs($service["name_en"],$data->$arrayid->status, "false" );
                $data->$arrayid->status = "false";
            }else{
                $pings = explode(" ", $str)[3];
                $pings_val = explode("/", $pings);
                $ping = $pings_val[1];
                send_notifs($service["name_en"], $data->$arrayid->status, "true", $ping );
                $data->$arrayid->status = "true";
                $data->$arrayid->lastping = round($ping, 0);
            }

            echo $arrayid . " " . $service["name_en"] . " " . $service["address"] . " " . $service["probe"] . $ping . "\n\n";
        }
    }
};
$data->lastcheck = date('d/m/y H:i');
$data->asXML($file);