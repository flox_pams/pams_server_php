<?php
function login_header(){
    header('WWW-Authenticate: Basic realm="Please log you in !"');
    header('HTTP/1.0 401 Unauthorized');
    echo '<p>Access denied. You did not enter a password.</p>';
    exit();
}

function do_login($settings){
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        login_header();
    }
    if ((md5($_SERVER['PHP_AUTH_PW']) == (string)$settings->admin_login->passwd_md5) && ($_SERVER['PHP_AUTH_USER'] == (string)$settings->admin_login->user)) {}else{
        login_header();
    }
}

?>