<?php
ob_start();
$settings = simplexml_load_file("internal/settings.xml");
//echo (string)$settings->admin_login->passwd_md5;
require("internal/admin_login.php");
do_login($settings);

if (isset($_GET["id"])){}else{
    header('Location: index.php?admin');
}
if ($_GET["lang"] == "fr") {
	$_SESSION['lang'] = "fr";
}
if ($_GET["lang"] == "en") {
	$_SESSION['lang'] = "en";
}
if (!isset($_SESSION['lang'])){
    $_SESSION['lang'] = (string)$settings->language->default_language;
}

//lang
$title_page = "Admin Area";
$send = "Submit";
$msgen = "Message in English";
$msgfr = "Message in French";
$nametxt = "Service Name";
$forceerr = "Service Mode";
$truetxt = "Yes";
$falsetxt = "No";
$sended1 = "The modification has been taken in account";
$mode_auto = "Automatic";
$mode_auto_ping = "Automatic with ping";
$mode_warn = "Warning without ping";
$mode_warn_ping = "Warning with ping";
$mode_maintenance = "Maintenance without ping";
$mode_maintenance_ping = "Maintenance with ping";
$mode_online = "Force: Online without ping";
$mode_online_ping = "Force: Online with ping";
$mode_offline = "Force: Offline";

if ($_SESSION['lang'] == "fr"){
    $title_page = "Espace administrateur";
	$send = "Valider";
	$msgen = "Message en Anglais";
	$msgfr = "Message en Francais";
	$nametxt = "Nom du service";
	$forceerr = "Mode de fonctionnement";
	$truetxt = "Oui";
	$falsetxt = "Non";
	$sended1 = "Les modification ont bien été prise en compte !";
	$mode_auto = "Automatique sans latence";
	$mode_auto_ping = "Automatique avec latence";
	$mode_warn = "Attention sans latence";
	$mode_warn_ping = "Attention avec latence";
	$mode_maintenance = "Maintenance sans latence";
	$mode_maintenance_ping = "Maintenance avec latence";
	$mode_online = "Forcer: En ligne sans latence";
	$mode_online_ping = "Forcer: En ligne avec latence";
	$mode_offline = "Forcer: Hors Ligne";
}

$file = "internal/data.xml";
$data = simplexml_load_file($file);
$serviceid = $_GET["id"];
//print_r($data->$serviceid);
//echo $serviceid;
require("static/static.php");

if ($_POST['action'] == 'true'){
	if ($_POST['msgen'] == ""){
		$data->$serviceid->msgen = '[none]';
	}else{
		$data->$serviceid->msgen = $_POST['msgen'];
	}

	if ($_POST['msgfr'] == ""){
		$data->$serviceid->msgfr = '[none]';
	}else{
		$data->$serviceid->msgfr = $_POST['msgfr'];
	}

	$data->$serviceid->mode = $_POST['forceerr'];

	$data->asXML($file);
	echo '<script>
	$.notify("&nbsp;' . $sended1 . '", {align:"right", verticalAlign:"top", type: "info", icon:"check", background: "#20D67B", close: true});
	</script>';
}


?>
<br>
<div class="flox-center flox-text-white flox-text-aligh-middle"><font size=50px><i class="fa fa-users-cog"></i>&nbsp;<?php echo $title_page; ?></font></div>
<br>
<div style="padding:1em;">
<div style="max-width:500px; margin-left: auto; margin-right: auto;">
	<style>
	.info {
		opacity: 0.8;
	}
	</style>

	<form id="contact" method="post" action="">
        <p><label class="flox-text-white flox-animate-zoom" for="nom"><?php echo $nametxt ?> :</label><input class="flox-input flox-round-xlarge flox-animate-left info" type="text" readonly="readonly" value="<?php echo $data->$serviceid->name_en; ?> - <?php echo $data->$serviceid->name_fr; ?>" /></p>
		<p>&nbsp;</p>
		<p><label class="flox-text-white flox-animate-zoom" for="nom"><?php echo $msgen ?> :</label><input class="flox-input flox-round-xlarge flox-animate-left" type="text" id="msgen" name="msgen" value="<?php echo $data->$serviceid->msgen; ?>"/></p>
		<p><label class="flox-text-white flox-animate-zoom" for="nom"><?php echo $msgfr ?> :</label><input class="flox-input flox-round-xlarge flox-animate-left" type="text" id="msgfr" name="msgfr" value="<?php echo $data->$serviceid->msgfr; ?>"/></p>
		<p><label class="flox-text-white flox-animate-zoom" for="nom"><?php echo $forceerr ?> :</label><select class="flox-input flox-round-xlarge flox-animate-left"  id="forceerr" name="forceerr">
			<option value="auto"><?php echo $mode_auto; ?></option>
			<option value="auto_ping"><?php echo $mode_auto_ping; ?></option>
			<option value="force_online"><?php echo $mode_online; ?></option>
			<option value="force_online_ping"><?php echo $mode_online_ping; ?></option>
			<option value="warn"><?php echo $mode_warn; ?></option>
			<option value="warn_ping"><?php echo $mode_warn_ping; ?></option>
			<option value="maintenance"><?php echo $mode_maintenance; ?></option>
			<option value="maintenance_ping"><?php echo $mode_maintenance_ping; ?></option>
			<option value="force_offline"><?php echo $mode_offline; ?></option>
		</select>
		</p>
		<p>&nbsp;</p>
		<input type="text" id="action" name="action" value="true" hidden/>
	<div style="text-align:center;"><input class="flox-button flox-white flox-round-xlarge flox-animate-bottom" type="submit" name="envoi" value="<?php echo $send; ?> !" /></div>
	</form>
	<script>document.getElementById("forceerr").value = "<?php echo $data->$serviceid->mode; ?>";</script>
</div>

<iframe id="spacer"
    width="1"
    height="50"
    src="about:blank"
    frameborder="0">
</iframe>