<?php
ob_start();
$settings = simplexml_load_file("internal/settings.xml");
//echo (string)$settings->admin_login->passwd_md5;
require("internal/admin_login.php");
do_login($settings);


if ($_GET["lang"] == "fr") {
	$_SESSION['lang'] = "fr";
}
if ($_GET["lang"] == "en") {
	$_SESSION['lang'] = "en";
}
if (!isset($_SESSION['lang'])){
    $_SESSION['lang'] = (string)$settings->language->default_language;
}

//lang
$title_page = "Admin Area - Advanced settings";
$send = "Submit";
$srv_settings = "Services Advanced Settings";
$sys_settings = "System Advanced Settings";
$sended1 = "The modification has been taken in account";

if ($_SESSION['lang'] == "fr"){
    $title_page = "Espace administrateur - Paramètres avancés";
	$send = "Valider";
	$srv_settings = "Paramètres avancés des services";
	$sys_settings = "Paramètres avancés du système";
	$sended1 = "Les modification ont bien été prise en compte !";
}

require("static/static.php");

if ($_POST['action'] == 'true'){
	file_put_contents("internal/data.xml",$_POST["services_settings"]);
	file_put_contents("internal/settings.xml",$_POST["system_settings"]);
	echo '<script>
	$.notify("&nbsp;' . $sended1 . '", {align:"right", verticalAlign:"top", type: "info", icon:"check", background: "#20D67B", close: true});
	</script>';
}


?>
<br>
<div class="flox-center flox-text-white flox-text-aligh-middle"><font size=50px><i class="fa fa-users-cog"></i>&nbsp;<?php echo $title_page; ?></font></div>
<br>
<div style="padding:1em;">
<div style="max-width:1500px; margin-left: auto; margin-right: auto;">
	<style>
	.info {
		opacity: 0.8;
	}
	textarea {
  		resize: none;
	}
	</style>

	<form id="settings" method="post" action="">
        <p><label class="flox-text-white flox-animate-zoom" for="services_settings"><?php echo $srv_settings; ?> :</label><?php echo $data->$serviceid->name_fr; ?>
		<textarea id="services_settings" name="services_settings" rows="20" cols="100" class="flox-input flox-round-xlarge flox-animate-left"><?php echo file_get_contents("internal/data.xml"); ?></textarea>
		</p>
		<p>&nbsp;</p>
		<p><label class="flox-text-white flox-animate-zoom" for="services_settings"><?php echo $sys_settings; ?> :</label><?php echo $data->$serviceid->name_fr; ?>
		<textarea id="system_settings" name="system_settings" rows="20" cols="100" class="flox-input flox-round-xlarge flox-animate-left"><?php echo file_get_contents("internal/settings.xml"); ?></textarea>
		</p>
		<input type="text" id="action" name="action" value="true" hidden/>
		<div style="text-align:center;"><input class="flox-button flox-white flox-round-xlarge flox-animate-bottom" type="submit" name="envoi" value="<?php echo $send; ?> !" /></div>
	</form>
</div>

<iframe id="spacer"
    width="1"
    height="50"
    src="about:blank"
    frameborder="0">
</iframe>