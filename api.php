<?php
ob_start();
$settings = simplexml_load_file("internal/settings.xml");
header('Content-Type: application/json');
$token = $settings->api->token_md5;
if ((md5($_GET["token"]) == $token) || (md5($_POST["token"]) == $token)){}else{
    $json["error_code"] = "pams_wrong_token";
    echo json_encode($json);
    exit();
}


$file = "internal/data.xml";
$data = simplexml_load_file($file);
$json_string = json_encode($data);
$array = json_decode($json_string, true);
$json_resp = array();
$cdnid = $distrib . $id;
$json_resp["check_code"] = (string)$settings->api->check_code;
$json_resp['infos']["lastcheck"] = (string)$data->lastcheck;
$json_resp['infos']["global_status"] = "pending";
$global_status = "ok";
foreach ($array as $service) {
    $arrayid = array_search($service, $array);
    $is_cat = explode("_", $arrayid)[0];
    if ($is_cat == 'cat'){
        $current_cat_id = $arrayid;
        $json_resp["categories"][$current_cat_id]["name_en"] = $service["name_en"];
        $json_resp["categories"][$current_cat_id]["name_fr"] = $service["name_fr"];
        
    }elseif($arrayid == "lastcheck"){
    }else{


    $lastping = $service["lastping"];
    if ($lastping > 900){
        $lastping = $lastping - 900;
    }

    $ping = "[disabled]";
    if ($service["mode"] == "auto_ping"){
        //mode auto avec ping
        if ($service["status"] == "true"){
            $ping = (integer)$lastping;
            $status = "ok";
        }else{
            $status = "alert";
        }
    }elseif ($service["mode"] == "auto"){
        //mode auto 
        if ($service["status"] == "true"){
            $status = "ok";
        }else{
            $status = "alert";
        }
    }elseif ($service["mode"] == "warn_ping"){
        //Mode warning avec ping
        $ping = (integer)$lastping;
        $status = "alert";
    }elseif ($service["mode"] == "warn"){
        //Mode warning
        $status = "warn";
    }elseif ($service["mode"] == "maintenance_ping"){
        //Mode warning avec ping
        $ping = (integer)$lastping;
        $status = "maintenance";
    }elseif ($service["mode"] == "maintenance"){
        //Mode warning
        $status = "maintenance";
    }elseif ($service["mode"] == "force_offline"){
        //mode offline
        $status = "alert";
    }elseif ($service["mode"] == "force_online_ping"){
        //mode en ligne avec ping
        $ping = (integer)$lastping;
        $status = "ok";
    }elseif ($service["mode"] == "force_online"){
        //mode en ligne
        $status = "ok";
    }
    $json_resp["categories"][$current_cat_id]["services"][$arrayid] = array("name_en" => $service["name_en"], "name_fr" => $service["name_fr"], "status" => $status, "last_ping" => $ping, "msg_en" => $service["msgen"], "msg_fr" => $service["msgfr"]);

    if ($global_status == "ok"){
        if ($status == "warn" or $status == "alert"){
            $global_status = $status;
        }
    }
    if ($global_status == "warn"){
        if ($status == "alert"){
            $global_status = $status;
        }
    }


    
    }
}
$json_resp['infos']["global_status"] = $global_status;
if (isset($_GET['xml'])){
    function xml_encode($mixed, $domElement=null, $DOMDocument=null) {
        if (is_null($DOMDocument)) {
            $DOMDocument =new DOMDocument;
            $DOMDocument->formatOutput = true;
            xml_encode($mixed, $DOMDocument, $DOMDocument);
            echo $DOMDocument->saveXML();
        }
        else {
            // To cope with embedded objects 
            if (is_object($mixed)) {
              $mixed = get_object_vars($mixed);
            }
            if (is_array($mixed)) {
                foreach ($mixed as $index => $mixedElement) {
                    if (is_int($index)) {
                        if ($index === 0) {
                            $node = $domElement;
                        }
                        else {
                            $node = $DOMDocument->createElement($domElement->tagName);
                            $domElement->parentNode->appendChild($node);
                        }
                    }
                    else {
                        $plural = $DOMDocument->createElement($index);
                        $domElement->appendChild($plural);
                        $node = $plural;
                    }
    
                    xml_encode($mixedElement, $node, $DOMDocument);
                }
            }
            else {
                $mixed = is_bool($mixed) ? ($mixed ? 'true' : 'false') : $mixed;
                $domElement->appendChild($DOMDocument->createTextNode($mixed));
            }
        }
    }
    echo xml_encode($json_resp);
}else{
    echo json_encode($json_resp);
}

?>